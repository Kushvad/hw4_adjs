const results= document.querySelector(".results");
const url = "https://ajax.test-danit.com/api/swapi/films";

function sendRequest(url) {
    return fetch(url).then((response) => response.json());
}

const request = sendRequest(url)
    .then((json) => responseMarkup(json))
    .then((array) => render(array));

function responseMarkup(json) {
    return json
        .sort((a, b) => a.episodeId - b.episodeId)
        .map(({ episodeId, name, openingCrawl, characters }) => {
            return getCharacters(characters).then((hero) => {
                const listItems = hero
                    .map(
                        (hero) =>
                            `<li class="name-item">${hero}</li>`
                    )
                    .join("");
                return `<h2>Episode ${episodeId}: ${name}</h2> 
                <p>${openingCrawl}</p>
                <p>Characters:</p>
                <ul class="names-list">${listItems}</ul>`;
            });
        });
}

function getCharacters(characters) {
    const promises = characters.map((characterUrl) =>
    sendRequest(characterUrl)
    );
    return Promise.all(promises).then((characters) => {
        return characters.map(({ name }) => name);
    });
}

function render(array) {
    Promise.allSettled(array).then((resolve) => {
        const ul = document.createElement("ul");
        ul.classList.add("films-list");
        results.append(ul);
        resolve.map((el) => {
            const li = document.createElement("li");
            li.innerHTML = el.value;
            ul.append(li);
        });
    });
}



